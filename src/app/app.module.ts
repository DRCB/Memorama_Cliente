import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MemoramaComponent } from './componentes/memorama/memorama.component';

import { CartaService } from './servicios/carta.service';
import { rutasApp } from './app.routing';



@NgModule({
  declarations: [
    AppComponent,
    MemoramaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rutasApp),
    HttpModule,
    FormsModule
  ],
  providers: [CartaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
